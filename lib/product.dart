import 'package:meta/meta.dart';

class Product {
  final String name;
  final int article;
  final price;
  final String image;

  Product({
    @required this.name,
    @required this.article,
    @required this.price,
    @required this.image,
  });
}
