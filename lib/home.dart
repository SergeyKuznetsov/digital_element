import 'package:digital_element/search_page.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 3), () {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (_) => SearchPage(),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      body: Center(
        child: Container(
          height: 181.48,
          width: 285,
          child: Stack(
            children: [
              Positioned(
                top: 0.0,
                left: 167.87,
                child: Container(
                  height: 26.96,
                  width: 27.42,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: Colors.white),
                ),
              ),
              Positioned(
                top: 33.54,
                left: 103,
                child: Container(
                  height: 77.6,
                  width: 78.91,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: Colors.white),
                ),
              ),
              Positioned(
                top: 136,
                child: Text(
                  'ЦИФРОВОЙ ЭЛЕМЕНТ',
                  style: TextStyle(
                      fontSize: 27,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                ),
              ),
              Positioned(
                top: 166.02,
                child: Text(
                  'Digital-инегратор с экспертизой в маркетинге',
                  style: TextStyle(fontSize: 13.3, color: Colors.white),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
