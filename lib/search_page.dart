import 'package:flutter/material.dart';
import 'package:digital_element/product.dart';
import 'package:digital_element/product_api.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  final productApi = ProductApi();
  List<Product> loadedProducts;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          'Список товаров',
          style: TextStyle(color: Colors.black, fontSize: 25),
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: FutureBuilder<List<Product>>(
            future: productApi.getProducts(),
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return Center(
                  child: errorWidget(),
                );
              }
              if (snapshot.hasData) {
                loadedProducts = snapshot.data;
                return productsWidget(
                  products: loadedProducts,
                );
              }
              return Center(
                child: loadingWidget(),
              );
            }),
      ),
    );
  }

  Widget productsWidget({@required List<Product> products}) {
    return GridView.builder(
        itemCount: products.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, childAspectRatio: 0.6),
        itemBuilder: (context, index) {
          return Column(
            children: [
              Expanded(
                flex: 6,
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Container(
                    child: Image.network(
                      products[index].image,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                    padding: EdgeInsets.only(left: 10),
                    alignment: Alignment.centerLeft,
                    child: Text('${products[index].article}')),
              ),
              Expanded(
                  flex: 2,
                  child: Container(
                    padding: EdgeInsets.only(left: 10),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      products[index].name,
                      style: TextStyle(fontSize: 20),
                    ),
                  )),
              Expanded(
                  child: Container(
                      padding: EdgeInsets.only(left: 10),
                      alignment: Alignment.centerLeft,
                      child: priceWidget(products: products, index: index))),
            ],
          );
        });
  }

  Widget priceWidget({@required List<Product> products, index}) {
    if (products[index].price.runtimeType == int ||
        products[index].price.runtimeType == double ||
        products[index].price.runtimeType == String) {
      return Text(
        '${products[index].price} руб.',
        style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
      );
    } else {
      return Text(
        'Товар отсутствует',
        style: TextStyle(fontSize: 17),
      );
    }
  }

  Widget loadingWidget() {
    return CircularProgressIndicator();
  }

  Widget errorWidget() {
    return Text('ошибка');
  }
}
