import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:digital_element/product.dart';
import 'package:digital_element/product_mapper.dart';

class ProductApi {
  Future<List<Product>> getProducts() async {
    final url = Uri.parse('https://d-element.ru/test_api.php');
    http.Response response = await http.get(url);
    //  print(response.body);

    if (response.statusCode == 200) {
      return ProductsMapper.fromMap(json.decode(response.body));
    } else {
      throw Exception('Error: ${response.reasonPhrase}');
    }
  }
}
