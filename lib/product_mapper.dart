import 'package:digital_element/product.dart';

extension ProductMapper on Product {
  static Product fromMap(Map map) {
    return Product(
        name: map['name'] as String,
        article: map['article'] as int,
        price: map['price'],
        image: map['image'] as String);
  }
}

extension ProductsMapper on List<Product> {
  static List<Product> fromMap(Map map) {
    return (map['items'] as List).map((e) => ProductMapper.fromMap(e)).toList();
  }
}
